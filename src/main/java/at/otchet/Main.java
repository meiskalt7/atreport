package at.otchet;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

public class Main {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/studprogress";

    static final String USER = "root";
    static final String PASS = "1234";

    String reportSource = "src/main/resources/Report.jrxml";
    String reportDest = "src/main/resources/Report.html";


    public static void main(String[] args) {
        Main main = new Main();
        main.createReport("География");
        main.createReport("История");
        main.createReport("Информатика");
        main.createReport("Математика");
    }

    Connection con;
    Statement stmt;
    ResultSet rs;

    public void createReport(String subject) {
        try {
            String query = "SELECT surname, st.name, s.name as subject, avg(mark) FROM students st, marks m, subjects s WHERE st.id = m.id_stud AND m.id_sub = s.id and s.name ='" + subject + "' GROUP BY st.id, s.name ORDER BY st.id, s.name;";
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            Map<String, Object> parameters = new HashMap<String, Object>();
            while (rs.next()) {
                parameters.put("surname", "surname");
                parameters.put("name", "name");
                parameters.put("subject", "subject");
                parameters.put("avg(mark)", "avg(mark)");
            }
            rs = stmt.executeQuery(query);
            JRResultSetDataSource rsdt = new JRResultSetDataSource(rs);
            JasperPrint jp;
            JasperReport jasperReport = JasperCompileManager.compileReport(reportSource);
            jp = JasperFillManager.fillReport(jasperReport, parameters, rsdt);
            JasperExportManager.exportReportToHtmlFile(jp, reportDest);
            JasperViewer jv = new JasperViewer(jp);
            jv.setVisible(true);
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
